package chargen

type Character struct {
	Name        string
	PlayerName  string
	Personality []string
	Ideals      []string
	Bonds       []string
	Flaws       []string
	Level       int
	Background  Background
	Alignment   Alignment
	Experience  int
	Inspiration int
	ArmorClass  int
	Initiative  int
	Speed       int
	HitPoints   HitPoints
	HitDice     HitDice
	Languages   []string
	Race        Race
	Class       Class
	Skills      []Skill
	Abilities   []Ability
	Equipment   Equipment
}

func InitCharacter() Character {
	return Character{
		Name:        "",
		PlayerName:  "",
		Personality: make([]string, 0),
		Ideals:      make([]string, 0),
		Bonds:       make([]string, 0),
		Flaws:       make([]string, 0),
		Level:       0,
		Background:  InitBackground(),
		Alignment:   NeutralNeutral,
		Experience:  0,
		Inspiration: 0,
		ArmorClass:  0,
		Initiative:  0,
		Speed:       0,
		HitPoints:   InitHitPoints(),
		HitDice:     InitHitDice(),
		Languages:   make([]string, 0),
		Race:        InitRace(),
		Class:       InitClass(),
		Skills:      InitSkills(),
		Abilities:   InitAbilities(),
		Equipment:   InitEquipment(),
	}
}
