package main

import (
	"ctom96/5e-cc/chargen"
	"fmt"
)

func main() {
	fmt.Println("Generating A Character...")
	newChar := chargen.InitCharacter()

	fmt.Println(newChar)

}

/*
	How to create a character as a human: (lvl 1)
	1. Pick a concept
	2. Pick a race
	3. Pick a class
	4. Pick a background
	5. Pick an Alignment
	6. Generate Ability Scores
	7. Determine Starting Equipment
	8. Generate Remaining Numbers (Health, AC, ETC)

	How to create a character as a computer: (lvl 1)
	1. Pick a race
	2. Pick a class
	3. Pick a background
	4. Pick an Alignment
	5. Generate Ability Scores
	6. Determine Starting Equipment
	7. Generate Remaining Numbers (Health, AC, ETC)
*/
