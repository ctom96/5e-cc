package chargen

type Feat struct {
	Name        string
	Description string
}
