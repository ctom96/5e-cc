package chargen

type Ability struct {
	Name     AbilityName
	Value    int
	Modifier int
}

type AbilityModifier struct {
	Ability      Ability
	Modification int
}

type AbilityName string

const (
	Strength     AbilityName = "Strength"
	Dexterity    AbilityName = "Dexterity"
	Constitution AbilityName = "Constitution"
	Intelligence AbilityName = "Intelligence"
	Wisdom       AbilityName = "Wisdom"
	Charisma     AbilityName = "Charisma"
)

func InitAbilities() []Ability {
	return []Ability{
		{Name: Strength, Value: 10, Modifier: 0},
		{Name: Dexterity, Value: 10, Modifier: 0},
		{Name: Constitution, Value: 10, Modifier: 0},
		{Name: Intelligence, Value: 10, Modifier: 0},
		{Name: Wisdom, Value: 10, Modifier: 0},
		{Name: Charisma, Value: 10, Modifier: 0},
	}
}
