package dice

import (
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

type DieRoll struct {
	numDice  int
	sides    int
	additive int
	rolls    []int
}

func (r DieRoll) Roll() int {
	var total int
	for i := 0; i < r.numDice; i++ {
		res := rollDie(r.sides)
		total += res
		r.rolls = append(r.rolls, res)
	}

	total += r.additive

	return total
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

// dieString is a string of type (XdN[+/-Y)* where:
// 		X is the number of dice
// 		N is the die type (d6, d10, d20, etc)
// 		+/-Y is an addition or substitution to the roll
func Roll(dieString string) int {
	i := strings.Index(dieString, "(")
	if i < 0 {
		currentRoll, err := extractRoll(dieString)
		if err != nil {
			fmt.Println(err)
			return 0
		}
		return currentRoll.Roll()
	}
	return 0
}

func extractRoll(dieString string) (DieRoll, error) {
	dieString = strings.ToLower(dieString)
	var currentRoll DieRoll

	// First-level of d stirng santization
	dLoc := strings.Index(dieString, "d")
	if dLoc < 0 {
		return currentRoll, errors.New("unrecognizeable die string")
	}

	var err error
	currentRoll.numDice, err = getNumDice(dieString)
	if err != nil {
		return currentRoll, err
	}

	currentRoll.sides, err = getNumSides(dieString)
	if err != nil {
		return currentRoll, err
	}

	currentRoll.additive, err = getAdditive(dieString)
	if err != nil {
		return currentRoll, err
	}

	return currentRoll, nil

}

func getNumDice(dieString string) (int, error) {
	dLoc := strings.Index(dieString, "d")
	if dLoc < 0 {
		return 0, errors.New("unable to find number of dice")
	}

	dString := dieString[0:dLoc]
	dNum, err := strconv.Atoi(dString)
	if err != nil {
		return 0, errors.New("die string unrecognizable")
	}

	return dNum, nil
}

func getNumSides(dieString string) (int, error) {
	dLoc := strings.Index(dieString, "d")
	if dLoc < 0 {
		return 1, errors.New("unable to find number of dice")
	}

	signLoc := strings.Index(dieString, "+")
	if signLoc < 0 {
		signLoc = strings.Index(dieString, "-")
	}
	if signLoc < 0 {
		signLoc = len(dieString)
	}

	sidesString := dieString[dLoc+1 : signLoc]
	sides, err := strconv.Atoi(sidesString)
	if err != nil {
		return 1, errors.New("unable to parse sides string")
	}

	return sides, nil
}

func getAdditive(dieString string) (int, error) {
	signLoc := strings.Index(dieString, "+")
	if signLoc < 0 {
		signLoc = strings.Index(dieString, "-")
	}
	if signLoc < 0 {
		return 0, nil
	}

	additiveStr := dieString[signLoc:]
	additive, err := strconv.Atoi(additiveStr)
	if err != nil {
		return 0, errors.New("unable to parse additive string")
	}

	return additive, nil
}

func rollDie(sides int) int {
	if sides <= 0 {
		return 0
	}
	return rand.Intn(sides) + 1
}
