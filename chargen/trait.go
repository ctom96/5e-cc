package chargen

// Alignment is some combination of (Lawful, Neutral, Chaotic) and (Good, Neutral, Evil)
type Alignment int

const (
	LawfulGood Alignment = iota
	NeutralGood
	ChaoticGood
	LawfulNeutral
	NeutralNeutral
	ChaoticNeutral
	LawfulEvil
	NeutralEvil
	ChaoticEvil
	None
)

// All creatures have a size rangig from Small to Gargantuan
type Size int

const (
	Small Size = iota
	Medium
	Large
	Huge
	Gargantuan
)

type HitPoints struct {
	Max     int
	Current int
	Temp    int
}

type HitDice struct {
	Number int
	Die    string
}

func InitHitPoints() HitPoints {
	return HitPoints{
		Max:     0,
		Current: 0,
		Temp:    0,
	}
}

func InitHitDice() HitDice {
	return HitDice{
		Number: 0,
		Die:    "d4",
	}
}
