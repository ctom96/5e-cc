package chargen

type Equipment struct {
	Money   Money
	Weapons []string
	Armor   []string
	Items   []string
}

type Money struct {
	Copper   int
	Silver   int
	Electrum int
	Gold     int
	Platinum int
}

func initMoney() Money {
	return Money{
		Copper:   0,
		Silver:   0,
		Electrum: 0,
		Gold:     0,
		Platinum: 0,
	}
}

func InitEquipment() Equipment {
	return Equipment{
		Money:   initMoney(),
		Weapons: make([]string, 0),
		Armor:   make([]string, 0),
		Items:   make([]string, 0),
	}
}
