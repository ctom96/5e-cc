package chargen

type Skill struct {
	Name       SkillName
	Ability    AbilityName
	Proficient bool
	Expertise  bool
}

type SkillName string

const (
	Acrobatics     SkillName = "Acrobatics"
	AnimalHandling SkillName = "Animal Handling"
	Arcana         SkillName = "Arcana"
	Athletics      SkillName = "Athletics"
	Deception      SkillName = "Deception"
	History        SkillName = "History"
	Insight        SkillName = "Insight"
	Intimidation   SkillName = "Intimidation"
	Investigation  SkillName = "Investigation"
	Medicine       SkillName = "Medicine"
	Nature         SkillName = "Nature"
	Perception     SkillName = "Perception"
	Performance    SkillName = "Performance"
	Persuasion     SkillName = "Persuasion"
	Religion       SkillName = "Religion"
	SleightOfHand  SkillName = "Sleight of Hand"
	Stealth        SkillName = "Stealth"
	Survival       SkillName = "Survival"
)

func InitSkills() []Skill {
	return []Skill{
		{Name: Acrobatics, Ability: Dexterity, Proficient: false, Expertise: false},
		{Name: AnimalHandling, Ability: Wisdom, Proficient: false, Expertise: false},
		{Name: Arcana, Ability: Intelligence, Proficient: false, Expertise: false},
		{Name: Athletics, Ability: Strength, Proficient: false, Expertise: false},
		{Name: Deception, Ability: Charisma, Proficient: false, Expertise: false},
		{Name: History, Ability: Intelligence, Proficient: false, Expertise: false},
		{Name: Insight, Ability: Wisdom, Proficient: false, Expertise: false},
		{Name: Intimidation, Ability: Charisma, Proficient: false, Expertise: false},
		{Name: Investigation, Ability: Intelligence, Proficient: false, Expertise: false},
		{Name: Medicine, Ability: Wisdom, Proficient: false, Expertise: false},
		{Name: Nature, Ability: Intelligence, Proficient: false, Expertise: false},
		{Name: Perception, Ability: Wisdom, Proficient: false, Expertise: false},
		{Name: Performance, Ability: Charisma, Proficient: false, Expertise: false},
		{Name: Persuasion, Ability: Charisma, Proficient: false, Expertise: false},
		{Name: Religion, Ability: Intelligence, Proficient: false, Expertise: false},
		{Name: SleightOfHand, Ability: Dexterity, Proficient: false, Expertise: false},
		{Name: Stealth, Ability: Dexterity, Proficient: false, Expertise: false},
		{Name: Survival, Ability: Wisdom, Proficient: false, Expertise: false},
	}
}
