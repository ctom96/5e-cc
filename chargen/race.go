package chargen

type Race struct {
	// Features are the sections of interesting information
	// Traits are the specific abilities of each race
	Name                string
	Description         string
	Features            map[string]string
	Cultures            []Culture
	AbilityModifiers    []AbilityModifier
	Age                 Age
	AlignmentTendencies []Alignment
	Size                Size
	Speed               int
	Languages           []string
	Skills              []Skill
	Feats               []Feat
	Traits              map[string]string
}

type Age struct {
	Adulthood int
	Lifespan  int
}

type Culture struct {
	Name        string
	Description string
	MaleNames   []string
	FemaleNames []string
}

func initAge() Age {
	return Age{
		Adulthood: 0,
		Lifespan:  0,
	}
}

func InitCulture() Culture {
	return Culture{
		Name:        "",
		Description: "",
		MaleNames:   make([]string, 0),
		FemaleNames: make([]string, 0),
	}
}

func InitRace() Race {
	return Race{
		Name:                "",
		Description:         "",
		Features:            make(map[string]string),
		Cultures:            make([]Culture, 0),
		AbilityModifiers:    make([]AbilityModifier, 0),
		Age:                 initAge(),
		AlignmentTendencies: make([]Alignment, 0),
		Size:                Medium,
		Speed:               30,
		Languages:           make([]string, 0),
		Skills:              make([]Skill, 0),
		Feats:               make([]Feat, 0),
		Traits:              make(map[string]string),
	}
}
